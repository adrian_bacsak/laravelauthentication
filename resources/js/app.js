//import './bootstrap';
//import * as Vue from 'vue'
//import App from '../views/App.vue'
/*
const app = Vue.createApp(App);

app.mount('#app')
*/
//window.Vue = require('vue');
//Vue.defineComponent('App', App);
import App from '../views/VueComponents/App.vue';
import Login from '../views/VueComponents/Login.vue';
import Register from '../views/VueComponents/Register.vue';
import Userlist from '../views/VueComponents/Userlist.vue';
import Changeemail from '../views/VueComponents/Changeemail.vue';
import Uploadimage from '../views/VueComponents/Uploadimage.vue';
import Genericindex from "../views/VueComponents/Genericindex.vue";
import { createApp } from 'vue'

const app = createApp({})
app.component('App', App);
app.component('Login', Login);
app.component('Register', Register);
app.component('Userlist', Userlist);
app.component('Changeemail', Changeemail);
app.component('Uploadimage', Uploadimage);
app.component('Genericindex', Genericindex);

app.mount('#app');
