@extends('../welcome')
@section('content')
<form method="POST" action="forgotten-password">
@csrf
    <div class="uk-width-1-3@m uk-align-center">
        <div class="uk-card uk-card-body uk-card-default">
            <label for="email">E-mail
                <input name="email" id="email" type="email" class="uk-input" />
            </label>
            <button type="submit" class="uk-button uk-button-primary uk-width-1-1@m">Küldés</button>
        </div>
    </div>
</form>
@endsection
