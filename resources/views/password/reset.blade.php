@extends('../welcome')
@section('content')
<form method="POST" action="{{route('password.change', ['token' => request('token')])}}">
    @csrf
    <div class="uk-card uk-card-default uk-card-body">
        <label for="email">E-mail:
            <input name="email" value="{{request('email')}}" id="email" type="email" class="uk-input" />
        </label>
        <label for="password">Jelszó:
            <input name="password" id="password" type="password" class="uk-input" />
        </label>
        <label for="password_confirmation">Jelszó mégegyszer:
            <input name="password_confirmation" id="password_confirmation" type="password" class="uk-input" />
        </label>

        <input hidden name="token" id="token" value="{{request('token')}}" />
    </div>

    <button type="submit" class="uk-input">Küldés</button>
</form>
@endsection
