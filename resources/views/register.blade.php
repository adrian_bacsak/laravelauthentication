@extends('welcome')
@section('content')
<Register>
    <template v-slot:csrf_field>@csrf</template>
    <template v-slot:name_error>
        @error('name')
            <div class="uk-alert uk-alert-danger">{{$message}}</div>
        @enderror
    </template>
    <template v-slot:email_error>
        @error('email')
            <div class="uk-alert uk-alert-danger">{{$message}}</div>
        @enderror
    </template>
    <template v-slot:password_error>
        @error('password')
            <div class="uk-alert uk-alert-danger">{{$message}}</div>
        @enderror
    </template>
    <template v-slot:confirm_password_error>
        @error('confirm_password')
            <div class="uk-alert uk-alert-danger">{{$message}}</div>
        @enderror
    </template>
</Register>
@endsection
