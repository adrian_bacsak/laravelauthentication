@extends('welcome')
@section('content')
@if(Auth::user()->email)
<Changeemail :user="{{Auth::user()}}">
    <template v-slot:required_fields>
        {{ method_field('PUT') }}
        @csrf
    </template>
    <template v-slot:new_email>
        @error('email')
            <div class="uk-alert uk-alert-danger">
                    {{$message}}
            </div>
        @enderror
    </template>
    <template v-slot:new_email_confirm>
         @error('email_confirmation')
            <div class="uk-alert uk-alert-danger">
                {{$message}}
            </div>
        @enderror
    </template>
</Changeemail>
@endif
@endsection
