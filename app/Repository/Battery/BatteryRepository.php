<?php

namespace App\Repository\Battery;

use App\Models\Battery;
use App\Repository\ICoreRepository;

class BatteryRepository implements ICoreRepository {
    protected $battery;

    public function __construct(Battery $battery) {
        $this->battery = $battery;
    }

    public function buildIndexData() {
        return $this->battery->paginate(25);
    }
}
