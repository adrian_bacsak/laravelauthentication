<?php

namespace App\Repository\User;

use App\Models\User;
use App\Repository\Contracts\ICoreRepository;

class UserRepository implements ICoreRepository {
    protected $user;

    public function __construct(User $user) {
        $this->user = $user;
    }

    public function buildIndexData() {
        return $this->user->all();
    }
}







?>
