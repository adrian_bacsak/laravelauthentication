<?php

namespace App\Repository\Contracts;

use App\Models\Contract;
use App\Repository\ICoreRepository;

class ContractsRepository implements ICoreRepository {
    protected $contract;

    public function __construct(Contract $contract) {
        $this->contract = $contract;
    }

    public function buildIndexData() {
        return $this->contract->paginate(25);
    }
}
