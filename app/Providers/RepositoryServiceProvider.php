<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Repository\ICoreRepository;
use App\Repository\User\UserRepository;
use App\Repository\Battery\BatteryRepository;
use App\Repository\Contracts\ContractsRepository;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(ICoreRepository::class, UserRepository::class);
        $this->app->bind(ICoreRepository::class, BatteryRepository::class);
        $this->app->bind(ICoreRepository::class, ContractsRepository::class);
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
