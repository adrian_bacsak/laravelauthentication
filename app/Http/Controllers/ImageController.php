<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\File;

class ImageController extends Controller
{
    public function show() {
        return view('image_upload');
    }

    public function store(Request $request) {
        $path = 'profile_images/'.auth()->user()->id;
        if(!File::exists($path))
            File::makeDirectory($path);

        Image::make($request->file('file'))->resize(1080, null,
        (function($constraint) {
            $constraint->aspectRatio();
        }))->save($path.'/1080.jpg');
        Image::make($request->file('file'))->resize(720, null,
        (function($constraint) {
            $constraint->aspectRatio();
        }))->save($path.'/720.jpg');
        Image::make($request->file('file'))->resize(360, null,
            (function($constraint)
                {$constraint->aspectRatio();
            })
            )->save($path.'/360.jpg');

        return redirect('/')->with('status', 'File uploaded successfully!');
    }
}
