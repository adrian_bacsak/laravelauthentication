<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Mail\PasswordReset;
use Illuminate\Support\Facades\Password;
use Illuminate\Support\Facades\Hash;
use App\Models\User;
class PasswordController extends Controller
{
    public function showForgottenPasswordForm() {
        return view('password/email');
    }

    public function resetPassword(Request $request) {
        $hashedPassword = Hash::make($request->password);
        $u = User::where('email', '=', $request->email)->first();

        if(Hash::check($request->password_confirmation, $hashedPassword)) {
            $u->password = $hashedPassword;

            $u->save();
        }

        return redirect('/')->with(['status' => 'Password changed successfuly']);
    }

    public function sendEmail(Request $request) {
        $email = $request->email;

        Password::sendResetLink(['email' => $email]);
        /*Mail::to($request->email)->send(new PasswordReset());
        return redirect('/')->with('status', 'Email has been sent!');

    */}
}
