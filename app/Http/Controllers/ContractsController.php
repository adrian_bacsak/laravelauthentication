<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repository\Contracts\ContractsRepository;

class ContractsController extends GenericController
{
    public function __construct(ContractsRepository $contractsRepository) {
        parent::__construct($contractsRepository);
    }

    public function index() {
        return parent::buildIndexData('contracts');
    }
}
