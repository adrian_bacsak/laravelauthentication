<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repository\Battery\BatteryRepository;

class BatteriesController extends GenericController
{
    public function __construct(BatteryRepository $batteryRepository) {
        parent::__construct($batteryRepository);
    }

    public function index() {
        return parent::buildIndexData('batteries');
    }
}
