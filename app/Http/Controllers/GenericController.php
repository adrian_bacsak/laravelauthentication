<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Repository\ICoreRepository;

class GenericController extends Controller
{
    private $coreRepository;

    public function __construct(ICoreRepository $coreRepository)
    {
        $this->coreRepository = $coreRepository;
    }

    public function buildIndexData($pageName) {
        $data = $this->coreRepository->buildIndexData();

        return view($pageName, ['data' => $data]);
    }
}
