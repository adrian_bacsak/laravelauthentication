<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Auth;
use App\Events\EmailUsed;
use App\Repository\User\UserRepository;

class UserController extends Controller
{
    private $userRepository;

    public function __construct(UserRepository $userRepository) {
        $this->userRepository = $userRepository;
    }

    public function GETUser($id) {
        $path = 'profile_images/'.$id;
        if(File::exists($path))
            return "<img src='/profile_images/$id/1080.jpg' />";
        else
            return "<img src='/profile_images/default.jpg' />";
        //return User::find($id)->first();
    }
    public function changeEmail(Request $request) {
        $u = User::where('id', '=', $request->id)->first();
        $responseMessage = 'Email did not change!';
        $validated = $request->validate([
            'email' => 'required|email|unique:users|confirmed',
            'email_confirmation' => 'required|email',
        ]);
        if($u) {
            if($request->email == $request->email_confirmation) {
                $u->email = $request->email;
                $u->save();
                $responseMessage = 'Email changed successfully!';
                broadcast(new EmailUsed(['emails' => User::all('email')]));
            }
        }
        return redirect('/')->with('status', $responseMessage);
    }

    public function showChangeEmailForm() {
        return view('change_email');
    }
    public function showUserList() {
        $u = $this->userRepository->buildIndexData();

        return view('user_list', ['users' => $u]);
    }
    public function showRegisterForm() {
        return view('register');
    }

    public function showLoginForm() {
        return view('login');
    }

    public function logout(Request $request) {
        $request->user()->tokens()->delete();
        auth()->logout();

        $request->session()->invalidate();

        $request->session()->regenerateToken();

        return redirect('/')->with('status', 'Sikeresen kijelentkeztél!');
    }

    public function login(Request $request) {
        $credentials  = ['email' => $request->email, 'password' => $request->password];
        $responseMessage = '';
        if(Auth::attempt($credentials)) {
            $request->session()->regenerate();
            $request->user()->createToken($request->email.':'.$request->password)->plainTextToken;

            $responseMessage = "Sikeresen bejelentkeztél!";
        } else {
            $responseMessage = "Sikertelen bejelentkezés!";
        }
        return redirect('/')->with('status', $responseMessage);
    }

    public function create(Request $request) {
        $validated = $request->validate([
            'name' => 'required|max:255',
            'password' => 'required',
            'email' => 'required',
            'confirm_password' => 'required',
        ]);

        $hashedPassword = Hash::make($request->password);

        $u = new User;

        if(Hash::check($request->password, $hashedPassword) && Hash::check($request->confirm_password, Hash::make($request->confirm_password))) {
            $u->password = $hashedPassword;
            $u->name = $request->name;
            $u->email = $request->email;

            $u->save();

            auth()->login($u);
        }

        return redirect('/')->with('status', 'Sikeresen regisztráltál!');
    }
}
