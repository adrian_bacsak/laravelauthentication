<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UserController;
use App\Http\Controllers\PasswordController;
use App\Http\Controllers\ImageController;

use App\Http\Controllers\BatteriesController;
use App\Http\Controllers\ContractsController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/asd', function() {
    return view('asd');
});

Route::group(['middleware' => ['guest']], function() {
    Route::get('/register', [UserController::class, 'showRegisterForm']);
    Route::post('/register', [UserController::class, 'create'])->name('postregister');
    Route::get('/login', [Usercontroller::class, 'showLoginForm']);
    Route::get('/forgotten-password', [PasswordController::class, 'showForgottenPasswordForm'])->name('forgotten-password-form');
    Route::get('/reset-password/{token}', function() {
        return view('password/reset', ['token' => request('token')]);
    })->name('password.reset');
    Route::post('/reset-password/{token}', [PasswordController::class, 'resetPassword'])->name('password.change');
    Route::post("/login", [UserController::class, 'login'])->name('login');
    Route::post('/forgotten-password', [PasswordController::class, 'sendEmail']);
});

Route::group(['middleware' => ['auth']], function() {
    Route::get('/logout', [Usercontroller::class, 'logout'])->name('logout');
    Route::get('/users', [UserController::class, 'showUserList'])->name('user_list');
    Route::get('/change-email', [UserController::class, 'showChangeEmailForm']);
    Route::put('/change-email/{id}', [UserController::class, 'changeEmail']);
    Route::get('/upload-image', [ImageController::class, 'show']);
});

Route::get('/teszt', function() {
    $img = Image::make('qwe.jpg');

    return $img->response();
});

Route::get('/batteries', [BatteriesController::class, 'index'])->name('batteries');
Route::get('/contracts', [ContractsController::class, 'index'])->name('contracts');